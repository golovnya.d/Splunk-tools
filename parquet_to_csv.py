import pandas as pd
import os
import re
from multiprocessing.dummy import Pool as ThreadPool

rootdir = '/Users/sh/Downloads/flow'
pool = ThreadPool(30)
full_name = []


def convert(f_name):
    if re.match('.*\.gz', f_name) or re.match('.*\.log\.parquet', f_name):
        print(f_name)
        df = pd.read_parquet(f_name)
        df.to_csv(f_name + '.csv')
        os.remove(f_name)


for root, subdirs, files in os.walk(rootdir):
    full_name = [root + '/' + x for x in files]
    pool.map(convert, full_name)
pool.close()
pool.join()
